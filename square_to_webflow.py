import xml.etree.ElementTree as ET
import csv

# replace with your path to xml file
tree = ET.parse("/Users/gbarker/Downloads/Squarespace-Wordpress-Export-01-18-2020.xml")
root = tree.getroot()

ns = {
	'excerpt': 'http://wordpress.org/export/1.2/excerpt/',
    'content': 'http://purl.org/rss/1.0/modules/content/',
	'wfw': 'http://wellformedweb.org/CommentAPI/',
	'dc': 'http://purl.org/dc/elements/1.1/',
	'wp': 'http://wordpress.org/export/1.2/',
}

# open a file for writing

webflowData = open('./webflow.csv', 'w', newline="")

# create the csv writer object

csvWriter = csv.writer(webflowData)

# get all of the possible authors
channel = root.find('channel', ns)
authors = []
print('getting authors')
for authorElement in channel.findall('wp:author', ns):
	author = []
	author.append(authorElement.find('wp:author_id', ns).text)
	author.append(authorElement.find('wp:author_login', ns).text)
	author.append(authorElement.find('wp:author_email', ns).text)
	author.append(authorElement.find('wp:author_display_name', ns).text)
	author.append(authorElement.find('wp:author_first_name', ns).text)
	author.append(authorElement.find('wp:author_last_name', ns).text)
	authors.append(author)
# print(authors)
print('\n')

# will store csv headers here
csvHeads = []

# do the csv items
count = 1
for itemElement in channel.findall('item'):
	# we only want blog items
	if hasattr(itemElement.find('link'), 'text'):
		if 'blog' not in itemElement.find('link').text:
			continue

	# get the csv headers first
	if count == 1:
		for header in itemElement.iter():
			headerText = header.tag
			csvHeads.append(headerText)
		csvWriter.writerow(csvHeads)

	item = []

	for csvHead in csvHeads:
		elem = itemElement.find(csvHead)

		if csvHead == 'item':
			item.append(count)
		elif hasattr(elem, 'text'):
			item.append(elem.text)
		else:
			item.append('')

	if len(item) > 0:
		csvWriter.writerow(item)
		count = count + 1 
webflowData.close()
print('finished %s blogs' % (count))